=====
ioctl
=====

Call arbitrary ioctls from the command line.

Building
========

``cc -o ioctl ioctl.c``

Usage
=====

.. code-block::

        Usage: ioctl [options] <device> <cmd> [<byte>...]

        Send ioctl with command <cmd> to <device>.

        Options:
          -h         Print help
          -v         Be verbose
          -q         Be quiet (overrides -v)
          -d <arg>   Pass a single direct argument to the ioctl
          -i         Interpret remaining arguments as an array of bytes
                     to be used to fill an I/O buffer. A pointer to this
                     buffer will be passed to the ioctl.
          -s <size>  Specify the size of the I/O buffer created by option -i.
                     If not specified, the size of the I/O buffer will be
                     equal to the number of bytes passed as arguments.
                     NOTE: The I/O buffer only gets created and sent to the
                     ioctl if the -i option is used with at least one byte.

Examples
========

Send ioctl 3 to /dev/device with an argument of 45
-------------------------------------------------

``ioctl /dev/device 3 -d 45``

Send ioctl 7 to /dev/device with a pointer to six consecutive bytes.
------------------------------------------------------------------

``ioctl /dev/device 7 -i 0x48 0x65 0x6C 0x6C 0x6F 0x0A``

Send ioctl 1 to /dev/device with a pointer to a buffer of size 25, where the first four bytes have specified values.
------------------------------------------------------------------------------------------------------------------

``ioctl /dev/device 1 -s 25 -i 0x41 0x6E 0x64 0x79``

