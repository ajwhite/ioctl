#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>

/* Flags */
int f_direct_arg = 0;
int f_byte_array = 0;
int f_io_size    = 0;
int f_verbose    = 0;
int f_quiet      = 0;

static void usage(char *name)
{
    fprintf(stderr,
            "Usage: %s [options] <device> <cmd> [<byte>...]\n"
            "\n"
            "Send ioctl with command <cmd> to <device>.\n"
            "\n"
            "Options:\n"
            "  -h         Print help\n"
            "  -v         Be verbose\n"
            "  -q         Be quiet (overrides -v)\n"
            "  -d <arg>   Pass a single direct argument to the ioctl\n"
            "  -i         Interpret remaining arguments as an array of bytes\n"
            "             to be used to fill an I/O buffer. A pointer to this\n"
            "             buffer will be passed to the ioctl.\n"
            "  -s <size>  Specify the size of the I/O buffer created by option -i.\n"
            "             If not specified, the size of the I/O buffer will be\n"
            "             equal to the number of bytes passed as arguments.\n"
            "             NOTE: The I/O buffer only gets created and sent to the\n"
            "             ioctl if the -i option is used with at least one byte.\n"
            "\n"
            ,name);
}


static void examples(char *name)
{
    fprintf(stderr,
            "Examples:\n"
            "# Send ioctl 3 to /dev/test with an argument of 45.\n"
            "    %s /dev/test 3 -d 45\n"
            "\n"
            "# Send ioctl 3 to /dev/test with a pointer to six consecutive bytes.\n"
            "    %s /dev/test 3 -i 0x48 0x65 0x6C 0x6C 0x6F 0x0A\n"
            "\n"
            "# Send ioctl 3 to /dev/test with a pointer to a buffer of size 25,\n"
            "# where the first four bytes have specified values.\n"
            "    %s /dev/test 3 -s 25 -i 0x41 0x6E 0x64 0x79\n"
            "\n"
            ,name, name, name);
}


static void print_bytes(uint8_t *buf, size_t buf_size)
{
    size_t index = 0;

    int rows = buf_size / 10;
    if (buf_size % 10)  /* Make sure to have enough rows for all bytes */
        rows += 1;

    printf("Index: +00  +01  +02  +03  +04  +05  +06  +07  +08  +09\n");
    for (int i = 0; i < rows; i++)
    {
        printf("%4d0: ", i);  /* Print tens value at beginning of row */

        for (int j = 0; j < 10 && index < buf_size; j++)
            printf("0x%02X ", buf[index++]);

        putchar('\n');
    }

    putchar('\n');
}


int main(int argc, char *argv[])
{
    char     *device_node   = NULL;
    int       fd            = 0;
    uint32_t  ioctl_arg     = 0;
    uint32_t  ioctl_cmd     = 0;
    size_t    io_buf_size   = 0;
    size_t    num_bytes     = 0;
    uint8_t  *ioctl_arg_buf = NULL;

    int c;
    while ((c = getopt(argc, argv, "d:is:qvh")) != -1)
    {
        switch (c) {
            /* Direct argument */
            case 'd':
                if (f_byte_array)
                {
                    fprintf(stderr, "%s: Option -d overrides option -i\n", argv[0]);
                    f_byte_array = 0;
                }
                f_direct_arg = 1;
                ioctl_arg = strtoul(optarg, NULL, 0);
                break;

            /* Byte array */
            case 'i':
                if (f_direct_arg)
                    fprintf(stderr, "%s: Option -d overrides option -i\n", argv[0]);
                else
                    f_byte_array = 1;
                break;

            /* Byte array size */
            case 's':
                f_io_size = 1;
                io_buf_size = strtoul(optarg, NULL, 0);
                break;

            /* Quiet */
            case 'q':
                f_quiet = 1;
                break;

            /* Verbose */
            case 'v':
                f_verbose = 1;
                break;

            /* Help */
            case 'h':
                usage(argv[0]);
                examples(argv[0]);
                return 0;

            /* Usage */
            case '?':
                usage(argv[0]);
                exit(1);
        }
    }

    /* Make sure we at least have the device node and ioctl command */
    if (optind + 1 > argc)
    {
        fprintf(stderr, "%s: too few arguments\n", argv[0]);
        usage(argv[0]);
        exit(1);
    }

    /* Open device node */
    device_node = argv[optind];
    fd = open(device_node, O_RDWR | O_SYNC);
    if (fd < 0)
    {
        fprintf(stderr, "%s: cannot open %s\n", argv[0], argv[optind]);
        exit(1);
    }
    optind++;

    /* Get ioctl command number */
    ioctl_cmd = strtol(argv[optind], NULL, 0);
    optind++;

    /* Create I/O buffer */
    if (f_byte_array)
    {
        /* The remaining arguments are all treated as bytes */
        num_bytes = argc - optind;

        if (!f_io_size)
            io_buf_size = num_bytes;

        ioctl_arg_buf = (uint8_t*)malloc(io_buf_size);

        if (!ioctl_arg_buf)
        {
            perror("malloc");
            close(fd);
            exit(1);
        }

        for (int i = 0; i < num_bytes; i++)
        {
            uint32_t tmp;

            if (i >= io_buf_size)
            {
                fprintf(stderr,
                        "Number of bytes specified exceeds size of allocated I/O buffer.\n"
                        "The byte array will be truncated to fit I/O buffer.\n");
                break;
            }

            tmp = strtoul(argv[optind], NULL, 0); /* Get byte from argv */
            ioctl_arg_buf[i] = (uint8_t)tmp;      /* Down-cast and copy byte to argument buffer */
            optind++;                             /* Move to next byte */
        }

        if (f_verbose && !f_quiet)
        {
            printf("Byte array before ioctl:\n");
            print_bytes(ioctl_arg_buf, io_buf_size);
        }
    }

    /* Send ioctl */
    int rc;
    if (f_direct_arg)
    {
        if (f_verbose && !f_quiet)
            printf("Sending ioctl to %s with command 0x%X and argument 0x%X\n\n", device_node, ioctl_cmd, ioctl_arg);
        rc = ioctl(fd, ioctl_cmd, ioctl_arg);
    }
    else if (f_byte_array)
    {
        if (f_verbose && !f_quiet)
            printf("Sending ioctl to %s with command 0x%X and pointer to buffer of size %lu\n\n", device_node, ioctl_cmd, io_buf_size);
        rc = ioctl(fd, ioctl_cmd, ioctl_arg_buf);
    }
    else
    {
        if (f_verbose && !f_quiet)
            printf("Sending ioctl to %s with command 0x%X\n\n", device_node, ioctl_cmd);
        rc = ioctl(fd, ioctl_cmd, NULL);
    }

    if (rc < 0)
    {
        fprintf(stderr, "Command 0x%X failed: %s\n", ioctl_cmd, strerror(errno));
        close(fd);
        exit(1);
    }

    if (f_byte_array && !f_quiet)
    {
        printf("Byte array after ioctl:\n");
        print_bytes(ioctl_arg_buf, io_buf_size);

        free(ioctl_arg_buf);
    }

    close(fd);
    return 0;
}
